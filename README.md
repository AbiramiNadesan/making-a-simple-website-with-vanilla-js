# Making a simple website with vanilla JS

- Webpage link: https://making-a-simple-website-with-vanilla-js-abiramin-d907492ee9db05.gitlab.io/
### Simple Website with Vanilla JS
- This code sets up a simple webpage with Bootstrap styling for a pricing table and a modal for order details. The JavaScript functions handle interactions with the modal and form submission.

- 1) The HTML structure defines a webpage with Bootstrap CSS and custom styles.

- 2) Bootstrap CSS and custom CSS are linked in the <head> section.

- 3) Bootstrap JavaScript (jquery.slim.min.js and popper.min.js) and custom JavaScript are linked at the end of the <body> section.

- 4) openModal(plan) function is called when a pricing button is clicked, opening the modal using Bootstrap's modal function.

- 5) highlightPlan(users) is intended to highlight the selected plan based on the number of users. However, this function is defined but not implemented.

- 6) openModal(plan) function is called when a pricing button is clicked, opening the modal using Bootstrap's modal function.

- 7) highlightPlan(users) is intended to highlight the selected plan based on the number of users. However, this function is defined but not implemented.

- 8) submitOrder() is called when the submit button in the modal is clicked. It populates an external form (not present in the provided HTML) with the user's details and closes the modal.

- 9) takeScreenshots() is a placeholder function to simulate taking screenshots and reporting Core Web Vitals. It displays an alert for now.

[![Simple Website with Vanilla JS](https://img.youtube.com/vi/vwTQ9IjoBmk/0.jpg)](https://www.youtube.com/watch?v=vwTQ9IjoBmk)


